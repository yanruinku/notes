// Author: Yanrui Hu
// Date: 2023/6/8
// Description: Add usage of accumulate and fill.
// Version: 0.1
// Keywords: accumulate, fill

// From LeeCode 1240. 铺瓷砖

// References:
// https://leetcode.cn/problems/tiling-a-rectangle-with-the-fewest-squares/solution/pu-ci-zhuan-by-leetcode-solution-r1bk/

#include <functional>
#include <vector>
#include <numeric>
using std::vector;
using std::accumulate;
using std::bit_or;

/* 判断 k*k 的矩阵中是否全是 false, bit_or 遇到 true之后， res就会是 true */
bool isAvailable(vector<vector<bool>> &rect, int x, int y, int k) {
  for (int i = 0; i < k; ++i) {
    bool res = accumulate(rect[x + i].begin() + y, rect[x + i].begin() + y + k, false, bit_or<bool>());
    if (res) return false;
  }
  return true;
}

/* 对 k*k 的矩阵进行填充，使用val作为填充值 */
void fillUp(vector<vector<bool>> &rect, int x, int y, int k, bool val) {
  for (int i = 0; i < k; ++i) {
    fill(rect[x + i].begin() + y, rect[x + i].begin() + y + k, val);
  }
}
