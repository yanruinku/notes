// Author: Yanrui Hu
// Date: 2022/10/15
// Description: About vector<string> in C language.
// Keywords: review, malloc, char **
// Version: 0.1
// Refenence: https://leetcode.cn/problems/build-an-array-with-stack-operations/

#include <malloc.h>
#include <string.h>

/**
 * Note: The returned array must be malloced, assume caller calls free().
 * `target`: List[int]
 * `targetSize`: used to describe the target_Array's size
 * `n`: particular number in the problem
 * `returnSize`: used to describe the ans_Array's size,
 *    with pointer type to be modified. [Useful and necessary for caller]
 */
char **buildArray(int target[], int targetSize, int n, int *returnSize) {
  const int kLimit = targetSize;
  char **ans = (char **)malloc(sizeof(char *) * 2 * n);

  /* for (int j = 0; j < 2 * limit; ++j) {
      ans[j] = (char *) malloc(sizeof(char) * 5);
  } */

  int idx = 0;
  int j = 0;
  for (int num = 1; num <= n; ++num) {
    if (kLimit == idx) break;

    ans[j] = (char *)malloc(sizeof(char) * 5); // max(sizeof("Push"), sizeof("Pop")) is 5
    strcpy(ans[j++], "Push");
    if (target[idx] == num) {
      ++idx;
    } else {
      ans[j] = (char *)malloc(sizeof(char) * 5);
      strcpy(ans[j++], "Pop");
    }
  }
  *returnSize = j;
  return ans;
}



#define STRINGLIZE(ivalue) (printf(#ivalue " is: %d\n", ivalue))

#define PUTI(i64val) \
  (printf("%d: $" #i64val ": %ld \n", __LINE__, (long)(i64val)))

int main(int argc, char* argv[]) {
  __auto_type str = "Push";
  // auto dd = "pop";
  PUTI(sizeof(str));
  PUT(123);
  // PUTI(sizeof(dd));

}
